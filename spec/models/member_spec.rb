# frozen_string_literal: true

require "rails_helper"
require "sidekiq/testing"

RSpec.describe Member, type: :model do
  describe "create" do
    it "fails without a name and a website" do
      err_msg = /NOT NULL constraint failed/
      expect { Member.create!(website: "http://example.com") }.to raise_error(ActiveRecord::NotNullViolation, err_msg)
    end
    it "requires a name and website" do
      err_msg = /NOT NULL constraint failed/
      expect { Member.create!(name: "test", website: "http://example.com") }.to_not raise_error
    end
  end

  describe "website" do
    before(:each) do
      @person_one = Member.new(name: "test", website: "http://example.com")
      @invalid_urls = %w[asdf 1234 xyz?rb]
      @valid_urls = %w[http://example.com]
    end
    it "catches invalid urls" do
      @invalid_urls.each do |url|
        @person_one.website = url
        err_msg = "Validation failed: Website #{url} is not a valid url"
        expect { @person_one.save! }.to raise_error(ActiveRecord::RecordInvalid, err_msg)
      end
    end
    it "allows valid urls" do
      @valid_urls.each do |url|
        @person_one.website = url
        expect { @person_one.save! }.to_not raise_error
      end
    end
    it "saves a shortened version of the website" do
      expect { @person_one.save }.to change { @person_one.shortened_urls.empty? }.from(true).to(false)
    end
    # TODO: cases where site is up but has no large headings? fail validation?
    xit "save h1-3 tags from the site" do
      proxy.stub(@person_one.website).and_return(text: site_with_headers)

      Sidekiq::Testing.fake!
      Sidekiq::Testing.inline! do
        # scrape the content
      end

      expect { task.run }.to change { @person.website_headers.empty? }.from(true).to(false)
    end
  end

  describe 'friends' do
    before(:each) do
      @person_one = Member.new(name: 'test', website: 'http://example.com')
      @person_two = Member.new(name: 'test', website: 'http://example.com')
    end
    xit 'should set up a bidirectional friendship' do
      expect(@person_one.friends).to eq([])
      expect(@person_two.friends).to eq([])

      @person_two.add_friend(@person_one)
      expect(@person_two.friends).to eq([@person_one])
      expect(@person_one.friends).to eq([@person_two])
    end
  end
end
