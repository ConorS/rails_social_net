# frozen_string_literal: true

require "rails_helper"

RSpec.describe "members/show", type: :view do
  before(:each) do
    @member = assign(:member, Member.create!(
                                name: "Name",
                                website: "http://some.valid.site"
                              ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match("http://some.valid.site")
  end
end
