# frozen_string_literal: true

require "rails_helper"

RSpec.describe "members/index", type: :view do
  before(:each) do
    assign(:members, [
             Member.create!(
               name: "Name",
               website: "http://some.valid.site"
             ),
             Member.create!(
               name: "Name",
               website: "http://some.valid.site"
             )
           ])
  end

  it "renders a list of members" do
    render
    assert_select "tr>td", text: "Name".to_s, count: 2
    assert_select "tr>td", text: "http://some.valid.site".to_s, count: 2
  end
end
