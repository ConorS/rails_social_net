## social media app

#### Requirements
* ruby 2.5.3
* Rails 5.2.2
* Redis (for sidekiq jobs)

### Startup
```
bundle install
bundle exec rails s
```

### Tests
```
bundle exec rspec
```

### Linter
```
bundle exec rubocop
```
