# frozen_string_literal: true

require "uri"

class Member < ApplicationRecord
  validate :valid_url?

  after_validation :scrape_headings # once url is valid, start scraping it
  after_save :shorten_website # shorten_website must be after save since relation requires id

  has_many :friendships
  has_many :friends, through: :friendships
  has_many :inverse_friendships, class_name: "Friendship", foreign_key: "friend_id"
  has_many :inverse_friends, through: :inverse_friendships, source: :user
  has_shortened_urls

  private

    def valid_url?
      uri = URI.parse(website)
      unless uri.is_a?(URI::HTTP) && !uri.host.nil?
        errors.add(:website, "#{website} is not a valid url")
      end
    rescue URI::InvalidURIError
      errors.add(:website, "#{website} is not a valid url")
      false
    end

    def shorten_website
      Shortener::ShortenedUrl.generate(website, owner: self)
    end

    def scrape_headings
      ScrapeHeadingsWorker.perform_async(website, id)
    end
end
