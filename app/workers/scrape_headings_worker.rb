# frozen_string_literal: true

class ScrapeHeadingsWorker
  include Sidekiq::Worker

  # @param [String] url
  # @param [String] member_id
  # @return [Array]
  def perform(url: nil, member_id: nil)
    # https://github.com/mperham/sidekiq/wiki/Best-Practices#1-make-your-job-parameters-small-and-simple
    # could pass model as param, but sidekiq params must be json serializable
    doc = Nokogiri::HTML(open(url, ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE))

    headings = %w[//h1 //h2 //h3].map do |tag|
      doc.xpath(tag)
    end

    saved_source = WebSource.create!(headings)

    member = Member.find(member_id)
    member.website_source = saved_source
    member.save!
  end
end
