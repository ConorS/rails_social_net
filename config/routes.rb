# frozen_string_literal: true

Rails.application.routes.draw do
  resources :friendships
  resources :members
  # want to list all shortened urls?
  get "/short/:id" => "shortener/shortened_urls#show"
  root "members#index"
end
