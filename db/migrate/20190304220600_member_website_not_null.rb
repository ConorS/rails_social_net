# frozen_string_literal: true

class MemberWebsiteNotNull < ActiveRecord::Migration[5.2]
  def change
    change_column :members, :website, :string, null: false
  end
end
