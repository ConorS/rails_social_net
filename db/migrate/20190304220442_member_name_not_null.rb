# frozen_string_literal: true

class MemberNameNotNull < ActiveRecord::Migration[5.2]
  def change
    change_column :members, :name, :string, null: false
  end
end
